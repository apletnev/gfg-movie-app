
const parentEl     = document.querySelector(".main");
const searchInput  = document.querySelector(".input");
const movieRatings = document.querySelector("#rating-select");
const movieGenres = document.querySelector("#genre-select");

let searchValue = "";
let ratings = 0;
let genre = "";
let filteredArrayOfMovies = [];

const URL = "https://movies-app.prakashsakari.repl.co/api/movies";

const getMovies = async () => {
    try{
        const {data} = await axios.get(URL);
        return data;
    }catch(err){

    }
};

let movies = await getMovies(URL);

// console.log(movies);

const createElement = (element) => document.createElement(element);

//function to create movie cards

const createMovieCard = (movies) => {
    for (let movie of movies) {
        
        // creating a parent container 
        const cardContainer = createElement("div");
        cardContainer.classList.add("card", "shadow");
        
        //creating image container
        const imageContainer = createElement("div");
        imageContainer.classList.add("card-image-container");
        
        //creating card image
        const imageElement = createElement("img");
        imageElement.classList.add("card-image");
        imageElement.setAttribute("src", movie.img_link);
        imageElement.setAttribute("alt", movie.name);
        
        imageContainer.appendChild(imageElement);
        
        cardContainer.appendChild(imageContainer);
        
        //creating card details container
        const cardDetails = createElement("div");
        cardDetails.classList.add("movie-details");
        
        //card title
        const titleEl = createElement("p");
        titleEl.classList.add("title");
        titleEl.innerText = movie.name;
        cardDetails.appendChild(titleEl);
        
        //card genre
        const genreEl = createElement("p");
        genreEl.classList.add("genre");
        genreEl.innerText = `Genre: ${movie.genre}`;
        cardDetails.appendChild(genreEl);        
        
        //ratings and length containers
        const movieRating = createElement("div");
        movieRating.classList.add("ratings");
        
        //star-rating component
        const ratings = createElement("div");
        ratings.classList.add("star-rating");
        
        //star icon
        const starIcon = createElement("span");
        starIcon.classList.add("material-icons-outlined");
        starIcon.innerText = "star";
        ratings.appendChild(starIcon);
        
        //rating
        const ratingEl = createElement("span");
        ratingEl.innerText = movie.imdb_rating;
        ratings.appendChild(ratingEl);
        movieRating.appendChild(ratings);
        
        //min
        const lengthEl = createElement("p");
        lengthEl.innerText = `${movie.duration} mins`;
        ratings.appendChild(lengthEl);

        movieRating.appendChild(lengthEl);
        cardDetails.appendChild(movieRating);
        cardContainer.appendChild(cardDetails);

        parentEl.appendChild(cardContainer);
    }
};

function getFilteredData(){
    filteredArrayOfMovies = searchValue?.length > 2 
    ? movies.filter(
        (movie) => 
            searchValue === movie.name.toLowerCase() || 
            searchValue === movie.director_name.toLowerCase() || 
            movie.writter_name.toLowerCase().split(",").includes(searchValue) ||
            movie.cast_name.toLowerCase().split(",").includes(searchValue) 
    ) 
    : movies; 
    if (ratings > 0){
        filteredArrayOfMovies = searchValue?.length > 2 ? filteredArrayOfMovies : movies;
        filteredArrayOfMovies = filteredArrayOfMovies.filter((movie) => movie.imdb_rating >= ratings);
    }
    if (genre?.length > 0){
        filteredArrayOfMovies = searchValue?.length > 0 || ratings > 7 ? filteredArrayOfMovies : movies;
        filteredArrayOfMovies = filteredArrayOfMovies.filter(movie => movie.genre.includes(genre));
    }
    return filteredArrayOfMovies;  
}

function handleSearch(event){
    searchValue = event.target.value.toLowerCase();
    let filterBySearch = getFilteredData();
    parentEl.innerHTML = "";
    createMovieCard(filterBySearch);
}

function debounce(callback, delay){
    let timerId;
    return (...args) => {
        clearTimeout(timerId);
        timerId = setTimeout(() => {callback(...args)}, delay);
    }
}

function handleRatingSelector(event){
    ratings = event.target.value;
    let filterByRating = getFilteredData();
    parentEl.innerHTML = "";
    createMovieCard(ratings ? filterByRating : movies);
} 

const debounceInput = debounce(handleSearch, 500);

searchInput.addEventListener("keyup", debounceInput)

movieRatings.addEventListener("change", handleRatingSelector);

//filter by genre

const genres = movies.reduce((acc, cur) => {
    let genresArr = [];
    let tempGenresArr = cur.genre.split(",");
    acc = [...acc, ...tempGenresArr];
    for (let genre of acc){
        if (!genresArr.includes(genre)){
            genresArr = [...genresArr, genre];
        }
    }
    return genresArr;
}, []);

for (let genre of genres){
    const option = createElement("option");
    option.classList.add("option");
    option.setAttribute("value", genre);
    option.innerText = genre;
    movieGenres.appendChild(option);
}

function handleGenreSelector(event){
    genre = event.target.value;
    const filteredByGenre = getFilteredData();
    parentEl.innerHTML = "";
    createMovieCard(genre ? filteredByGenre : movies);
} 

movieGenres.addEventListener("change", handleGenreSelector);


createMovieCard(movies);